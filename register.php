<?php
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!--style css-->
  <link rel="stylesheet" href="dist/css/style.css">
</head>
<body class="hold-transition h-100 w-100 register login-box">
<div class="login-box h-100 w-35 ml-auto mr-auto">
  <div class="card ">
    <div class="card-body login-card-body">
      <div class="row text-center">
        <h3><a href="./index.php" id="backA" style="colo"><i class="ion ion-arrow-left-c"></i></a></h3>
        <h3 class="login-box-msg ml-auto mr-auto">Register</h3>
      </div>

      <form action="." method="post">
        <div class="d-flex">
          <div class="input-group mb-3 pr-3">
            <input type="text" class="form-control input-outline-green " placeholder="Name">
            <div class="input-group-append">
              <div class="input-group-text input-outline-green">
                <span class="fas fa-user-edit"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="text" class="form-control input-outline-green " placeholder="Lastname">
            <div class="input-group-append">
              <div class="input-group-text input-outline-green">
                <span class="fas fa-user-edit"></span>
              </div>
            </div>
          </div>
        </div>

        <div class="d-flex">
          <div class="input-group mb-3 pr-3">
            <input type="text" class="form-control input-outline-green" placeholder="username">
            <div class="input-group-append">
              <div class="input-group-text input-outline-green">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <!-- select -->
            <select class="form-control input-outline-green">
              <option>ประเภทผู้ใช้</option>
              <option>นิสิต</option>
              <option>อาจารย์</option>
              <option>บุคลากร</option>
            </select>
            <div class="input-group-append">
              <div class="input-group-text input-outline-green">
                <span class="fas fa-globe"></span>
              </div>
            </div>

          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" class="form-control input-outline-green" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text input-outline-green">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control input-outline-green" placeholder="Confirm Password">
          <div class="input-group-append">
            <div class="input-group-text input-outline-green">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-8 mb-3 ">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 text-center mb-3">
            <button type="submit" class="btn btn-warning btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
  </div>
</div>

</body>
</html>
