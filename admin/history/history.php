<?php ?>

<?php ?>
<?php ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบยืม-คืนอุปกรณ์</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

  <link href="../../dist/css/style.css" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include "../view/top-bar.php";?>

  <?php include "../view/side-bar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper overflow-auto" style="height: 90vh;">
    <!-- Content Header (Page header) -->
    <div class="content-header ">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">ประวัติการยืม-คืน</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../index">Home</a></li>
              <li class="breadcrumb-item active">ประวัติการยืม-คืน</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!--Modal Form-->
    <div class="modal fade" id="addEquipment">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Large Modal</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row pt-1 pb-1">
              <div class="col-12 p-1 text-center">
                <img class="img-thumbnail col-3" src="../../dist/img/AdminLTELogo.png">
              </div>
              <div class="col-12 text-center">
                <a href="#">upload</a>
              </div>
            </div>
            <div class="row pt-1 pb-1">
              <div class="col-2 p-1 text-right">
                <label><b>ชื่ออุปกรณ์ : &nbsp;</b></label>
              </div>
              <div class="col-8">
                <input type="text" placeholder="ชื่ออุปกรณ์" class="col-12 p-1">
              </div>
            </div>
            <div class="row pt-1 pb-2">
              <div class="col-2 p-1 text-right">
                <label><b>หมวดหมู่ : &nbsp;</b></label>
              </div>
              <div class="col-8">
                <select class="p-1">
                  <option>---เลือกหมวดหมู่---</option>
                  <option>หมวดหมู่ทดลอง</option>
                </select>
              </div>
            </div>
            <div class="row pt-1 pb-1">
              <div class="col-2 p-1 text-right">
                <label><b>รายละเอียด : &nbsp;</b></label>
              </div>
              <div class="col-8">
                <textarea type="text" rows="4" placeholder="รายละเอียดอุปกรณ์" class="col-12 p-1"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--/Modal Form-->

    <!-- Main content -->
    <section class="content overflow-auto">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-4">
            <!-- small box -->
            <div class="small-box bg-gray-medium">
              <div class="inner">
                <h3>50</h3>

                <p>รายการยืมทั้งหมด</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-list"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-4">
            <!-- small box -->
            <div class="small-box bg-gray-medium">
              <div class="inner">
                <h3>50</h3>

                <p>รายการอุปกรณ์ที่คืนแล้วทั้งหมด</p>
              </div>
              <div class="icon">
                <i class="ion ion-clipboard"></i>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-12">

              <!--borrow equipment table-->
              <div class="card">
                <div class="card-header bg-info d-flex align-items-center" style="height: 65px">
                  <h3 class="card-title" ><i class="fas fa-cube mr-1"></i>ประวัติการยืม-คืน</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="equipmentState" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ลำดับ</th>
                      <th>รหัสครุภัฑณ์</th>
                      <th>ชื่ออุปกรณ์</th>
                      <th>หมวดหมู่</th>
                      <th width="150">สถานะ</th>
                      <th width="300">จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>A001-001</td>
                      <td>Arduino R3</td>
                      <td>หมวดหมู่ทดลอง</td>
                      <td style="text-align: center">
                        <a class="btn btn-success btn-sm col-sm-6" href="#">
                          กำลังยืม
                        </a>
                      </td>
                      <td style="text-align: center">
                        <a class="btn btn-primary btn-sm" href="#">
                          <i class="fas fa-folder">
                          </i>
                          View
                        </a>
                        <a class="btn btn-info btn-sm" href="#">
                          <i class="fas fa-pencil-alt">
                          </i>
                          Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="#">
                          <i class="fas fa-trash">
                          </i>
                          Delete
                        </a>
                      </td>
                    </tr>

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <?php include "../view/footer.php";?>
  </div>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../../plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="../../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/moment.min.js"></script>
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $("#equipmentState").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $("#equipmentSend").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $("#requestTable").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

</body>
</html>


