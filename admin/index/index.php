<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../../plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

  <link href="../../dist/css/style.css" rel="stylesheet">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <?php include "../view/top-bar.php";?>

  <?php include "../view/side-bar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper overflow-auto" style="height: 80vh;">
    <!-- Content Header (Page header) -->
    <div class="content-header ">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Home</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../index">Home</a></li>
              <li class="breadcrumb-item active">ระบบจัดการอุปกรณ์</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6 ">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>25</h3>

                <p>อุปกรณ์ที่กำลังยืม</p>
              </div>
              <div class="icon">
                <i class="ion ion-cube"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>50</h3>

                <p>อุปกรณ์ที่รอการส่งมอบ</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-checkbox-outline"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>44</h3>

                <p>คำขอที่รออนุมัติ</p>
              </div>
              <div class="icon">
                <i class="ion ion-document-text"></i>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main content -->
        <section class="content">
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="card">
                <div class="card-header d-flex align-items-center bg-gradient-red" style="height: 65px">
                  <h3 class="card-title">
                    <i class="fas fa-chart-pie mr-1"></i>
                    สถิติการยืม-คืนอุปกรณ์
                  </h3>
                  <div class="card-tools">
                  </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content p-0">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active" id="revenue-chart"
                         style="position: relative; height: 300px;">
                      <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                    </div>
                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                      <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                    </div>
                  </div>
                </div><!-- /.card-body -->
              </div>
              <!-- /.card -->

            </section>
            <!-- /.Left col -->
            <!-- right col -->
          </div>
          <!-- /.row (main row) -->
          <div class="row">
            <div class="col-12">

              <!--borrow equipment table-->
              <div class="card">
                <div class="card-header bg-info d-flex align-items-center" style="height: 65px">
                  <h3 class="card-title" ><i class="fas fa-cube mr-1"></i>อุปกรณ์ที่กำลังยืม</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="equipmentState" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>รหัสครุพันธ์</th>
                      <th>ชื่ออุปกรณ์</th>
                      <th>หมวดหมู่</th>
                      <th>รหัสผู้ยืม</th>
                      <th>วันที่เริ่มยืม</th>
                      <th>กำหนดวันคืน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>B1112</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503887</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1540</td>
                      <td>Arduino Mega</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503621</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B0002</td>
                      <td>Motor</td>
                      <td>อุปกรณ์อิเล็กทรอนิกส์</td>
                      <td>b6020500321</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1342</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020501325</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1143</td>
                      <td>จอคอมพิวเตอร์</td>
                      <td>อุปกรณ์คอมพิวเตอร์</td>
                      <td>b6020501111</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1344</td>
                      <td>สาย HTMI</td>
                      <td>อุปกรณ์คอมพิวเตอร์</td>
                      <td>b6020503887</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                      <td style="text-align: center">
                        1/5/2563
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!--Wait equipment table-->
              <div class="card">
                <div class="card-header bg-warning d-flex align-items-center" style="height: 65px">
                  <h3 class="card-title"><i class="fas fa-check-circle mr-1"></i>อุปกรณ์ที่รอการส่งมอบ</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="equipmentSend" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>รหัสครุพันธ์</th>
                      <th>ชื่ออุปกรณ์</th>
                      <th>หมวดหมู่</th>
                      <th>รหัสผู้ยืม</th>
                      <th>กำหนดวันรับของ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>B1122</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503887</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1123</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503542</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1445</td>
                      <td>Arduino Ax</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020500123</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                    </tr>
                    <tr>
                      <td>B1234</td>
                      <td>แป้นพิมพ์</td>
                      <td>อุปกรณ์คอมพิวเตอร์</td>
                      <td>b6020500201</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!--Status Request Table-->
              <div class="card">
                <div class="card-header bg-success d-flex align-items-center" style="height: 65px">
                  <h3 class="card-title"><i class="fas fa-edit mr-1"></i>คำขอที่รอการอนุมัติ</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="requestTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>รหัสครุพันธ์</th>
                      <th>ชื่ออุปกรณ์</th>
                      <th>หมวดหมู่</th>
                      <th>รหัสผู้ยืม</th>
                      <th>วันที่ส่งคำขอ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>B1122</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503887</td>
                      <td style="text-align: center">
                        1/1/2563
                    </tr>
                    <tr>
                      <td>B1122</td>
                      <td>Arduino Uno</td>
                      <td>อุปกรณ์ IOT</td>
                      <td>b6020503887</td>
                      <td style="text-align: center">
                        1/1/2563
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <?php include "../view/footer.php";?>
  </div>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../../plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../../plugins/sparklines/sparkline.js"></script>
<!-- jQuery Knob Chart -->
<script src="../../plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../../plugins/moment/moment.min.js"></script>
<script src="../../plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../../plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../../plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $("#equipmentState").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $("#equipmentSend").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $("#requestTable").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

</body>
</html>
