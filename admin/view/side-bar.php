<?php ?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary color-theme-darkblack ิ">
  <!-- Brand Logo -->
  <a href="." class="brand-link color-theme-green">
    <img
      src="https://www.pngitem.com/pimgs/m/37-378005_borrowlogo-standard-borrow-car-subscription-logo-hd-png.png"
      alt="LOGO Image" class="brand-image img-thumbnail elevation-3 "
      style="opacity: .8;">
    <span class="brand-text font-weight-light">Borrow System</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar ">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a class="d-block cursor-hand">นาย วรวุฒิ พันธุสิทธิ์เสรี<br>
          (Admin)
        </a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="../index" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>
              Home
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              จัดการอุปกรณ์
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="../equipment/typeEquipment.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>จัดการหมวดหมู่</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="../equipment/nameEquipment.php" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>จัดการอุปกรณ์</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="../request/requestManage.php" class="nav-link">
            <i class="nav-icon fas fa-file"></i>
            <p>
              จัดการคำขอ
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="../history/history.php" class="nav-link">
            <i class="nav-icon fas fa-history"></i>
            <p>
              ประวัติการยืม-คืน
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="../manage/manage.php" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>
              จัดการผู้ดูแลระบบ
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="." class="nav-link">
            <i class="nav-icon fas fa-exchange-alt"></i>
            <p>
              สลับเป็นผู้ใช้
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="." class="nav-link">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              ออกจากระบบ
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
